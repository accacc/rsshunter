package us.congcong.rsshunter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RsshunterApplication {

	public static void main(String[] args) {
		SpringApplication.run(RsshunterApplication.class, args);
	}
}
