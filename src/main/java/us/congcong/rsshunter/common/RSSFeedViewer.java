package us.congcong.rsshunter.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.feed.AbstractRssFeedView;

import com.rometools.rome.feed.rss.Channel;
import com.rometools.rome.feed.rss.Content;
import com.rometools.rome.feed.rss.Item;

import us.congcong.rsshunter.model.SampleChannel;
import us.congcong.rsshunter.model.SampleContent;

@Component("RSSFeedViewer")
public class RSSFeedViewer extends AbstractRssFeedView {

   @Override
   protected void buildFeedMetadata(Map<String, Object> model, Channel feed,
      HttpServletRequest request) {

	  SampleChannel sampleChannel = (SampleChannel) model.get("feedChannel");
      feed.setTitle(sampleChannel.getTitle());
      feed.setDescription(sampleChannel.getDescription());
      feed.setLink(sampleChannel.getLink());

      super.buildFeedMetadata(model, feed, request);
   }

   @Override
   protected List<Item> buildFeedItems(Map<String, Object> model,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
   
      List<SampleContent> listContent = (List<SampleContent>) model.get("feedContent");
      List<Item> items = new ArrayList<Item>(listContent.size());

      for(SampleContent tempContent : listContent ){

         Item item = new Item();

         Content content = new Content();
         content.setValue(tempContent.getSummary());
         item.setContent(content);

         item.setTitle(tempContent.getTitle());
         item.setLink(tempContent.getUrl());
         item.setPubDate(tempContent.getCreatedDate());

         items.add(item);
      }

      return items;		
   }
}